<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class LuUser extends Model implements
AuthenticatableContract, AuthorizableContract {

    use Authenticatable,
        Authorizable;

    protected $table = 'lu_user';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password', 'reg_ip', 'avatar', 'nickname', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'reg_ip'
    ];
    public $timestamps = true;

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

}
