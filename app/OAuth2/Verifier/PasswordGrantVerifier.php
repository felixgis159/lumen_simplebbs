<?php

namespace App\OAuth2\Verifier;

use App\Models\LuUser;

class PasswordGrantVerifier {

    public function verify($username, $password)
    {
        $user = LuUser::where('username', $username)->first();
        if ($user['password'] == sha1($password)) {
            return $user['id'];
        }
        return false;
    }

}
