<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */


$app->post('oauth/access_token', function() {
    return response()->json(app('oauth2-server.authorizer')->issueAccessToken());
});
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->get('user', ['middleware' => 'api.auth', 'as' => 'user', 'uses' => 'App\Http\Controllers\UserController@index']);
    $api->get('posts', function () {
        // This route does not require authentication.
    });
});
