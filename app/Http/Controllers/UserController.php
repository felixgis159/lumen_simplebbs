<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LuUser;

class UserController extends Controller {

    public function index()
    {
        return LuUser::all();
    }

    public function show(Request $request)
    {
        echo $request->user()->id;
    }

}
