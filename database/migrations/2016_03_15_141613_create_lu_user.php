<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLuUser extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lu_user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 50);
            $table->string('password', 50);
            $table->string('nickname', 50)->default('');
            $table->string('avatar', 255)->default('');
            $table->timestamps();

            $table->unique('username', 'username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lu_user');
    }

}
